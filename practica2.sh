#!/bin/bash
clear
cat ascii.bmp

AMARILLO='\033[1;33m'
SINCOLOR='\033[0m'

log() {
#Leemos de la entrada estandar la ruta y nombre de un fichero de log y lo mostramos

bien=0
while [ "$bien" -eq "0" ]
do
	echo "Introduce la ruta del fichero"
	read ruta
	ruta=$(realpath $ruta)
		if [ ! -d "$ruta" ]
		then
			echo "$ruta no es un directorio válido o no existe, vuelva a intentarlo"
		else
			bien=1
		fi
done

bien=0
while [ "$bien" -eq "0" ]
do
	echo "Introduce el nombre del fichero"
	read nombre
	fichero=$ruta"/"$nombre
		if [ ! -f "$fichero" ]
		then
			echo "$fichero no es un fichero válido o no existe, vuelva a intentarlo"
		else
			bien=1
		fi
done
cat $fichero
echo -e "${AMARILLO}==============FIN=============="
echo -e "Pulsa enter para volver al menú.${SINCOLOR}"
read
}

cadena() {
#Leemos de la entrada estandar un fichero y un patron y buscamos este en el fichero
echo "Introduce la cadena de texto a buscar"
read cadena

bien=0
while [ "$bien" -eq "0" ]
do
	echo "Introduce el fichero donde quieres buscar la cadena"
	read fichero
		if [ ! -f "$fichero" ]
		then
			echo "$fichero no es un fichero válido o no existe, vuelva a intentarlo"
		else
			bien=1
		fi
done

grep -F "$cadena" $fichero > /dev/null

if [ "$?" -eq "1" ]
then
	echo "El texto indicado no se ha encontrado en el fichero $fichero"
else
	echo "El fichero contiene el texto indicado"
fi

echo -e "${AMARILLO}==============FIN=============="
echo -e "Pulsa enter para volver al menú.${SINCOLOR}"
read
}

paquete() {
#Leemos el nombre de un paquete y buscamos si dicho paquete esta instalado o no
echo "Introduce el paquete que desea saber si se encuentra instalado en el sistema"
read paquete

which $paquete > /dev/null

if [ "$?" -eq "1" ]
then
	echo "El paquete $paquete no está instalado o no existe"
else
	echo "El paquete $paquete está instalado en:"
	which $paquete
fi

echo -e "${AMARILLO}==============FIN=============="
echo -e "Pulsa enter para volver al menú.${SINCOLOR}"
read
}

usuarios() {
#Mostramos todos los usuarios logados en el sistema
echo -e "${AMARILLO}Listado de usuarios logados en el sistema${SINCOLOR}"
w | sed '1,2d' | cut -f1 -d" "
echo -e "${AMARILLO}==============FIN=============="
echo -e "Pulsa enter para volver al menú.${SINCOLOR}"
read
}

linea() {
#Leemos el nombre de un fichero y mostramos linea a linea el contenido del fichero añadiendo delante de cada linea eln texto Linea(num)
echo "Introduce el fichero que vamos a listar"
read fichero
echo -e "${AMARILLO}Fichero $fichero"
echo -e "================${SINCOLOR}"
num=1
while read linea
do
	echo "Linea $num: $linea"
	let num=$num+1
done < $fichero
echo -e "${AMARILLO}==============FIN=============="
echo -e "Pulsa enter para volver al menú.${SINCOLOR}"
read
}

PS3="¿Opción?: "

echo "Elige una opción"

#Creamos opciones del menu en un array
opciones=("Mostrar log" "Buscar cadena" "Buscar paquete" "Usuarios logados" "Linea a linea" "Salir")

#Mostramos menu y enlazamos a cada opcion
select menu in "${opciones[@]}"
do
	case $menu in
		"Mostrar log") log ;;
		"Buscar cadena") cadena ;;
		"Buscar paquete") paquete ;;
		"Usuarios logados") usuarios ;;
		"Linea a linea") linea ;;
		"Salir") exit ;;
	esac
done
